﻿using MatTreeView.Engine.Models;
using MatTreeView.Engine.Models.DTO;
using MatTreeView.Engine.Repositories;
using Microsoft.AspNetCore.Mvc;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MatTreeView.Engine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private readonly GroupRepository _groupRepository;

        public GroupsController(GroupRepository groupRepository)
        {
            _groupRepository = groupRepository;
        }

        [HttpPost("add-group")]
        public async Task<IActionResult> AddGroup([FromBody] GroupDto group)
        {
            if (group == null) return BadRequest("Invalid input request.");

            var groupToSave = new Group();
            groupToSave.Name = group.Name;
            groupToSave.GroupCatagoryId = group.GroupCatagoryId;
            groupToSave.IsDelete = group.IsDelete;

            var groups = await _groupRepository.GetGroups(0);
            groupToSave.Code = "G-" + groups.Count;
            return Ok(await _groupRepository.AddGroup(groupToSave));
        }

        [HttpPost("Add-Group-Catagory")]
        public async Task<IActionResult> AddGroupCatagory([FromBody] GroupCatagoryDto groupCatagoryDto)
        {
            if (groupCatagoryDto == null) return BadRequest("Invalid input request.");

            var groupCatagory = new GroupCatagory();
            //groupCatagory.Code = groupCatagoryDto.Code;
            groupCatagory.Name = groupCatagoryDto.Name;
            groupCatagory.ParentId = groupCatagoryDto.ParentId;

            var catagories = await _groupRepository.GetGroupCatagories();
            groupCatagory.Code = "GC-" + catagories.Count;

            return Ok(await _groupRepository.AddGroupCatagory(groupCatagory));

        }

        [HttpGet("get-groups")]
        public async Task<IActionResult> GetGroups(long? catagoryId)
        {
            return Ok(await _groupRepository.GetGroups(catagoryId));
        }

        [HttpGet("get-group-catagories")]
        public async Task<IActionResult> GetGroupCatagories()
        {
            var catagories = await _groupRepository.GetGroupCatagories();
            if (catagories.Count != null && catagories.Count > 0)
            {
                var data = catagories.Where(c => c.ParentId == null);
                return Ok(data);
            }
            return Ok(catagories);
        }
    }
}
