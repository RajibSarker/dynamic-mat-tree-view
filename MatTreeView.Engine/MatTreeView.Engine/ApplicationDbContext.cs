﻿using MatTreeView.Engine.Models;
using Microsoft.EntityFrameworkCore;

namespace MatTreeView.Engine
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
            //AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        #region tables
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupCatagory> GroupCatagories { get; set; }
        #endregion
    }
}
