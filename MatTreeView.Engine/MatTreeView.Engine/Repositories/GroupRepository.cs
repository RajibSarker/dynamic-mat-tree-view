﻿using MatTreeView.Engine.Models;
using Microsoft.EntityFrameworkCore;

namespace MatTreeView.Engine.Repositories
{
    public class GroupRepository
    {
        private readonly ApplicationDbContext _context;

        public GroupRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Group> AddGroup(Group group)
        {
            if(group == null) return null;

            await _context.Groups.AddAsync(group);
            var isSaved = await _context.SaveChangesAsync() > 0;

            if(isSaved) return group;
            return null;
        }

        public async Task<GroupCatagory> AddGroupCatagory(GroupCatagory groupCatagory)
        {
            if (groupCatagory == null) return null;

            await _context.GroupCatagories.AddAsync(groupCatagory);
            var isSaved = await _context.SaveChangesAsync()>0;
            if(isSaved) return groupCatagory;
            return null;
        }

        public async Task<ICollection<Group>> GetGroups(long? catagoryId)
        {
            if(catagoryId!=null && catagoryId > 0)
            {
                return await _context.Groups.Where(c=>c.GroupCatagoryId == catagoryId).AsNoTracking().OrderBy(c=>c.Id).ToListAsync();
            }
            else
            {
                return await _context.Groups.AsNoTracking().OrderBy(c=>c.Id).ToListAsync();
            }
        }

        public async Task<ICollection<GroupCatagory>> GetGroupCatagories()
        {
            return await _context.GroupCatagories.Include(c=>c.Groups).OrderBy(c=>c.Id).ToListAsync();
        }
    }
}
