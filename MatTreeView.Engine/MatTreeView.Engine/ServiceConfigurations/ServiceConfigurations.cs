﻿using MatTreeView.Engine;
using Microsoft.EntityFrameworkCore;

namespace BlogManagementSystem.Configurations
{
    public class ServiceConfigurations
    {
        public static void Configuration(IServiceCollection service, IConfiguration configuration)
        {
            // npgsql configurations
            service.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("AppConnectionString"));
            });

            // registered services
            //service.AddTransient<IPostManager, PostManager>();
            //service.AddTransient<IPostRepository, PostRepository>();
        }
    }
}
