﻿namespace MatTreeView.Engine.Models
{
    public class Group
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? GroupCatagoryId { get; set; }
        public GroupCatagory GroupCatagory { get; set; }
        public bool IsDelete { get; set; }
    }
}
