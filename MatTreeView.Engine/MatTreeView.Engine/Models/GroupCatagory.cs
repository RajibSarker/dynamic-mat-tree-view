﻿namespace MatTreeView.Engine.Models
{
    public class GroupCatagory
    {
        public GroupCatagory()
        {
            GroupCatagories = new List<GroupCatagory>();
            Groups = new List<Group>();
        }
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public GroupCatagory Parent { get; set; }
        public bool IsDelete { get; set; }

        public List<GroupCatagory> GroupCatagories { get; set; }
        public ICollection<Group> Groups { get; set; }
    }
}
