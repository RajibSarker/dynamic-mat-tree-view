﻿namespace MatTreeView.Engine.Models.DTO
{
    public class GroupCatagoryDto
    {
        public GroupCatagoryDto()
        {
            GroupCatagories = new List<GroupCatagoryDto>();
            Groups = new List<GroupDto>();
        }
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public bool IsDelete { get; set; }

        public ICollection<GroupCatagoryDto> GroupCatagories { get; set; }
        public ICollection<GroupDto> Groups { get; set; }
    }
}
