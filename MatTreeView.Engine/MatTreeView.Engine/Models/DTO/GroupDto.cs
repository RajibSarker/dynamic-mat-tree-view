﻿namespace MatTreeView.Engine.Models.DTO
{
    public class GroupDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? GroupCatagoryId { get; set; }
        public bool IsDelete { get; set; }
    }
}
