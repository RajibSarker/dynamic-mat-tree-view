import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Group } from '../_models/group';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  baseUrl = 'https://localhost:7197/api/';
  groups: Group[] = [];
  constructor(private http: HttpClient) { }

  // getGroups(): Group[] {
  //   this.groups = [];

  //   var group = new Group();
  //   group.groupId = 12;
  //   group.groupCatagoryId = 1;
  //   group.code = 'a';
  //   group.groupName = 'aa';

  //   var groupa = new Group();
  //   groupa.groupId = 15;
  //   groupa.groupCatagoryId = 1;
  //   groupa.code = 'b';
  //   groupa.groupName = 'bb';
  //   this.groups.push(group);
  //   this.groups.push(groupa);

  //   return this.groups;
  // }

  getGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.baseUrl + 'Groups/get-groups');
  }

  addGroup(group: Group): Observable<Group>{
    return this.http.post<Group>(this.baseUrl + 'Groups/Add-group', group);
  }
}
