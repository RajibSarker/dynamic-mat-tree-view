import { Injectable } from "@angular/core";
import { DynamicFlatNode, GroupCategoryNGroupNode } from "../_models/DynamicFlatNode";
import { Group } from "../_models/group";
import { GroupCatagory } from "../_models/group-category";
import { GroupCategoryService } from "./group-category.service";
import { GroupService } from "./group.service";

@Injectable({ providedIn: 'root' })
// export class DynamicDatabase {
//   dataMap = new Map<string, string[]>([
//     ['Fruits', ['Apple', 'Orange', 'Banana']],
//     ['Vegetables', ['Tomato', 'Potato', 'Onion']],
//     ['Apple', ['Fuji', 'Macintosh']],
//     ['Onion', ['Yellow', 'White', 'Purple']],
//   ]);

//   rootLevelNodes: string[] = ['Fruits', 'Vegetables'];

//   /** Initial data from database */
//   initialData(): DynamicFlatNode[] {
//     return this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true));
//   }

//   getChildren(node: string): string[] | undefined {
//     return this.dataMap.get(node);
//   }

//   isExpandable(node: string): boolean {
//     return this.dataMap.has(node);
//   }
// }

export class DynamicDatabase {
  data = new Map<string | number, GroupCategoryNGroupNode[]>([]);
  clientId = 0;
  clientLegalName = 'Mat Tree view';
  // rootLevelNodes: number[] = [null, 2, 3, 4, 14];
  rootLevelNodes: string[] = [' '];
  private groups: Group[] = [];
  private rootData: DynamicFlatNode[] = [];
  private catagories: GroupCatagory[] = [];
  // constructor(private groupService: GroupService, private groupCatagoryService: GroupCategoryService) {

  //   // getting group catagories
  //   this.groupCatagoryService.getGroupCatagories().subscribe((res: GroupCatagory[]) => {
  //     this.catagories = res;
  //   }, error => {
  //     console.log(error);
  //   }, () => {
  //     this.groupService.getGroups().subscribe((res: Group[]) => {
  //       this.groups = res
  //     }, error => {

  //     }, () => {
  //       const rootGroups = this.groups.filter(group => group.groupCatagoryId === null);
  //       let categoryNodes: GroupCategoryNGroupNode[] = new Array();
  //       let groupNodes: GroupCategoryNGroupNode[] = new Array();
  //       if (this.catagories === null || this.catagories === undefined || this.catagories.length <= 0) {
  //         if (rootGroups !== null && rootGroups !== undefined && rootGroups.length > 0) {
  //           this.clientLegalName = 'Mat Tree view';
  //         }
  //       } else {
  //         this.clientLegalName = 'Mat Tree view';
  //       }

  //       console.log('groups ' + rootGroups);
  //       this.catagories.forEach(child => {
  //         categoryNodes.push(new GroupCategoryNGroupNode(child.code + ' ' + child.name, true, child));
  //       });

  //       if (rootGroups.length === 0) {
  //       } else {
  //         rootGroups.forEach(child => {
  //           groupNodes.push(new GroupCategoryNGroupNode(child.code + ' ' + child.groupName, false, child));
  //         });
  //       }

  //       groupNodes.forEach(element => {
  //         categoryNodes.push(element);
  //       });

  //       this.data.set(this.clientLegalName, [...categoryNodes]);
  //       this.catagories.forEach(element => {
  //         this.recusive(element, element.groupCatagories.length);
  //       });

  //       console.log('data show in complete method');
  //       console.log(this.data);
  //     })
  //   });

  //   // this.catagories = this.groupCatagoryService.getGroupCatagoriesByClientId();
  //   // this.groups = this.groupService.getGroups();
  // }



  constructor(private groupService: GroupService, private groupCatagoryService: GroupCategoryService) {

    this.groupCatagoryService.getGroupCatagories().subscribe(result => {
      console.log('hit');
      this.catagories = result;

      if (this.catagories === null || this.catagories === undefined || this.catagories.length <= 0) {
        this.catagories = [];
      }
    }, error => {

      // this.alertifyService.error('Sorry could not get catagories!');
    }, () => {

      this.groupService.getGroups().subscribe(result => {

        this.groups = result;
      }, error => {

        //  this.alertifyService.error('Sorry could not get group catagories!');
      }, () => {
        const rootGroups = this.groups.filter(group => group.groupCatagoryId === null);
        let categoryNodes: GroupCategoryNGroupNode[] = new Array();
        let groupNodes: GroupCategoryNGroupNode[] = new Array();
        if (this.catagories === null || this.catagories === undefined || this.catagories.length <= 0) {
          if (rootGroups !== null && rootGroups !== undefined && rootGroups.length > 0) {
            this.clientLegalName = 'Mat Tree view';
          }
        } else {
          this.clientLegalName = 'Mat Tree view';
        }
        console.log('groups ' + rootGroups);
        this.catagories.forEach(child => {

          categoryNodes.push(new GroupCategoryNGroupNode(child.code + ' ' + child.name, true, child));
        });

        if (rootGroups.length === 0) {

        } else {

          rootGroups.forEach(child => {
            groupNodes.push(new GroupCategoryNGroupNode(child.code + ' ' + child.name, false, child));
            //groupName.push(child.groupName);
          });
        }
        groupNodes.forEach(element => {

          categoryNodes.push(element);
        });
        this.data.set(this.clientLegalName, [...categoryNodes]);
        this.catagories.forEach(element => {

          this.recusive(element, element.groupCatagories.length);
        });

        console.log('data show in complete method');
        console.log(this.data);

      });
    });

  }

  recusive(groupCatagories: GroupCatagory, length: number) {
    if (length === 0) {
      this.mapData(groupCatagories);
      return;
    } else {
      this.mapData(groupCatagories);
      groupCatagories.groupCatagories.forEach(element => {
        return this.recusive(element, element.groupCatagories.length);
      });

    }
  }

  mapData(element: GroupCatagory) {
    const groupsOfThisCategory = this.groups.filter(group => group.groupCatagoryId === element.id);


    let childCategoryNodes: GroupCategoryNGroupNode[] = new Array();
    let groupNodes: GroupCategoryNGroupNode[] = new Array();

    if (element.groupCatagories.length === 0) {
      ;
    } else {
      element.groupCatagories.forEach(childCategory => {
        childCategoryNodes.push(new GroupCategoryNGroupNode(childCategory.code + ' ' + childCategory.name, true, childCategory));
      });
    }

    if (groupsOfThisCategory.length === 0) {
      ;
    } else {

      groupsOfThisCategory.forEach(child => {
        groupNodes.push(new GroupCategoryNGroupNode(child.code + ' ' + child.name, false, child));
        //groupName.push(child.groupName);
      });
    }
    this.data.set(element.code + ' ' + element.name, [...childCategoryNodes, ...groupNodes]);

  }
  // dataMap = new Map<any, any[]>([
  //     ['Fruits', ['Apple', 'Orange', 'Banana']],
  //     ['Vegetables', ['Tomato', 'Potato', 'Onion']],
  //     ['Apple', ['Fuji', 'Macintosh']],
  //     ['Onion', ['Yellow', 'White', 'Purple']]
  // ]);

  /** Initial data from database */
  //   async initialData(): Promise<DynamicFlatNode[]>  {
  //         return await this.getRoot();
  //     }
  initialData(rootName: string): DynamicFlatNode[] {
    this.rootLevelNodes[0] = rootName;
    var rootAsFakeCategory = new GroupCategoryNGroupNode('', true, null);
    return this.rootLevelNodes.map(name => new DynamicFlatNode(name, 0, true, rootAsFakeCategory));
  }
  async getRoot(): Promise<DynamicFlatNode[]> {
    const root: number[] = [];
    // t catagories: GroupCatagory[];
    const catagories = await this.groupCatagoryService.getGroupCatagoriesByAsync();
    catagories.forEach(element => {
      if (root.indexOf(element.parentId as number) !== -1) {
        ;
      } else {
        root.push(element.parentId as number);
      }
    });


    return root.map(categoryParentId => new DynamicFlatNode(categoryParentId, 0, true));;
  }
  getChildren(node: string | number): GroupCategoryNGroupNode[] | undefined {
    return this.data.get(node);
  }

  isExpandable(node: string | number): boolean {
    var childrens = this.getChildren(node);
    if (childrens == null) {
      return false;
    }
    else {
      return childrens.length > 0;
    }

  }

  getGroupNode(group: Group): GroupCategoryNGroupNode {
    const groupNode = new GroupCategoryNGroupNode(group.code + ' ' + group.name, false, group);
    return groupNode;
  }
  getCatagoryNode(groupCatagory: GroupCatagory): GroupCategoryNGroupNode {
    const groupCatagoryNode = new GroupCategoryNGroupNode(groupCatagory.code + ' ' + groupCatagory.name, true, groupCatagory);
    return groupCatagoryNode;
  }
}