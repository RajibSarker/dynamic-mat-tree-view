import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GroupCatagory } from '../_models/group-category';

@Injectable({
  providedIn: 'root'
})
export class GroupCategoryService {

  baseUrl = 'https://localhost:7197/api/';

  groupCategories: GroupCatagory[] = [];
  constructor(private http: HttpClient) { }

  // getGroupCatagoriesByClientId(): GroupCatagory[] {
  //   this.groupCategories = [];
  //   var category = new GroupCatagory();
  //   category.id = 1;
  //   category.code = 'A';
  //   category.name = 'AA';

  //   this.groupCategories.push(category);
  //   return this.groupCategories;
  // }

  getGroupCatagories(): Observable<GroupCatagory[]> {
    return this.http.get<GroupCatagory[]>(this.baseUrl + 'Groups/get-group-catagories');
  }
  saveGroupCatagory(groupCatagory: GroupCatagory){
    return this.http.post(this.baseUrl + 'Groups/Add-Group-Catagory', groupCatagory);
  }

  async getGroupCatagoriesByAsync(): Promise<GroupCatagory[]> {
    return await this.http.get<GroupCatagory[]>(this.baseUrl + 'Groups/get-group-catagories').toPromise();
  }
}
