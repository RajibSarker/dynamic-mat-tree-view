export class GroupCatagory {
    id: number = 0;
    name: string = '';
    code: string = '';
    parentId: number | null = 0;
    groupCatagories: GroupCatagory[] = [];
    constructor() {
        this.code = '';
        this.groupCatagories = new Array();
    }
}