// export class DynamicFlatNode {
//   constructor(
//     public item: string,
//     public level = 1,
//     public expandable = false,
//     public isLoading = false,
//   ) { }
// }

export class DynamicFlatNode {
  constructor(public item: number | string, public level = 1, public expandable = false, public data?: GroupCategoryNGroupNode,
     public isExpanded = false, public isLoading = false) { }
}


export class GroupCategoryNGroupNode {
  constructor(public name: string, public isGroupCategory = false, public data: any = null) { }
}