export class Group{
    id: number = 0;
    name: string = '';
    description: string = '';
    code: string = '';
    isDelete: boolean;
    groupCatagoryId: number = 0;
}