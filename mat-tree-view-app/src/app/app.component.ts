import { FlatTreeControl } from '@angular/cdk/tree';
import { Component } from '@angular/core';
import { DynamicDataSource } from './_models/DynamicDataSource';
import { DynamicFlatNode } from './_models/DynamicFlatNode';
import { DynamicDatabase } from './_services/DynamicDatabase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mat-tree-view-app';

  constructor(database: DynamicDatabase) {
    // this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
    // this.dataSource = new DynamicDataSource(this.treeControl, database);

    // this.dataSource.data = database.initialData();
  }

  // treeControl: FlatTreeControl<DynamicFlatNode>;

  // dataSource: DynamicDataSource;

  // getLevel = (node: DynamicFlatNode) => node.level;

  // isExpandable = (node: DynamicFlatNode) => node.expandable;

  // hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;
}
