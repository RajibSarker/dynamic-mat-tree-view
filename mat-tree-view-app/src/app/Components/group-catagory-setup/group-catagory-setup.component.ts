import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GroupCatagory } from 'src/app/_models/group-category';
import { DynamicDatabase } from 'src/app/_services/DynamicDatabase';
import { GroupCategoryService } from 'src/app/_services/group-category.service';

@Component({
  selector: 'app-group-catagory-setup',
  templateUrl: './group-catagory-setup.component.html',
  styleUrls: ['./group-catagory-setup.component.css']
})
export class GroupCatagorySetupComponent implements OnInit {

  // tslint:disable: no-debugger
  @Input()
  groupCatagory: GroupCatagory = new GroupCatagory();
  groupSetupForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<GroupCatagorySetupComponent>, @Inject(MAT_DIALOG_DATA) public data: GroupCatagory,
    public groupCatagoryService: GroupCategoryService,
    private route: Router, private database: DynamicDatabase,
  ) {
  }

  ngOnInit() {
    this.groupSetupForm = this.createForm();
  }

  // group create form
  createForm() {
    return this.fb.group({
      groupCatagoryNameControl: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],

    });
  }
  // submit form
  onSubmit() {

    this.groupCatagory.name = this.groupSetupForm.get('groupCatagoryNameControl')?.value;
    this.groupCatagory.parentId = this.data.parentId;
    this.groupCatagory.code = this.data.code;
    this.groupCatagoryService.saveGroupCatagory(this.groupCatagory).subscribe(result => {
      // this.alertify.success('Group category added successfully.');
      this.dialogRef.close(result);
    }, error => {
      console.log(error);
      this.dialogRef.close('error');
    }
    );
  }
  onCloseCancel() {
    this.dialogRef.close('Cancel');
  }
}
