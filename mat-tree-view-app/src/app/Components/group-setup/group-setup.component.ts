import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GroupCategoryNGroupNode } from 'src/app/_models/DynamicFlatNode';
import { Group } from 'src/app/_models/group';
import { DynamicDatabase } from 'src/app/_services/DynamicDatabase';
import { GroupService } from 'src/app/_services/group.service';

@Component({
  selector: 'app-group-setup',
  templateUrl: './group-setup.component.html',
  styleUrls: ['./group-setup.component.css']
})
export class GroupSetupComponent implements OnInit {

  isHasDeliveries = false;
  group: Group = new Group();
  //membersId: User[] = new Array();
  public data = [];
  public settings = {};
  public selectedItems = [];
  uris: string[] = new Array();
  selectedUris: string[] = new Array();

  constructor(
    private groupService: GroupService,
    private route: Router,
    public dialogRef: MatDialogRef<GroupSetupComponent>, @Inject(MAT_DIALOG_DATA) public groupData: Group,
    private database: DynamicDatabase
  ) { }

  ngOnInit() {
    // //this.getMembers();
    // // get all delivery
    // this.getDeliveries();
    // // setting and support i18n
    // this.settings = {
    //   singleSelection: false,
    //   idField: 'id',
    //   textField: 'fullName',
    //   enableCheckAll: true,
    //   selectAllText: 'Select All',
    //   unSelectAllText: 'Unselect All',
    //   allowSearchFilter: true,
    //   limitSelection: -1,
    //   clearSearchFilter: true,
    //   maxHeight: 197,
    //   // itemsShowLimit: 3,
    //   searchPlaceholderText: 'Search Member',
    //   noDataAvailablePlaceholderText: 'Members Not Found!',
    //   closeDropDownOnSelection: false,
    //   showSelectedItemsAtTop: false,
    //   defaultOpen: false
    // };
  }
  public onFilterChange(item: any) {
    console.log(item);
  }
  public onDropDownClose(item: any) {
    console.log(item);
  }

  public onItemSelect(item: any) {
    console.log(item);
  }
  public onDeSelect(item: any) {
    console.log(item);
  }

  public onSelectAll(items: any) {
    console.log(items);
  }
  public onDeSelectAll(items: any) {
    console.log(items);

  }
  saveGroup() {
    this.group.groupCatagoryId = this.groupData.groupCatagoryId;
    this.group.code = this.groupData.code;
    this.groupService.addGroup(this.group).subscribe((data: Group) => {
      let groupNode: GroupCategoryNGroupNode;
      groupNode = this.database.getGroupNode(data);
      // this.membersId = [];
      this.dialogRef.close(data);
      // this.route.navigate(['/group-list']);
    }, error => {
      //this.membersId = [];
      console.log(error);
    });

  }
  onCloseCancel() {
    this.dialogRef.close('Cancel');
  }
}
