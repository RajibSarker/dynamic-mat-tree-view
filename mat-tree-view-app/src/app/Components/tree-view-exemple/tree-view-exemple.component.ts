import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DynamicFlatNode } from 'src/app/_models/DynamicFlatNode';
import { Group } from 'src/app/_models/group';
import { GroupCatagory } from 'src/app/_models/group-category';
import { DynamicDatabase } from 'src/app/_services/DynamicDatabase';
import { GroupCategoryService } from 'src/app/_services/group-category.service';
import { GroupService } from 'src/app/_services/group.service';
import { DynamicDataSource } from '../dynamicDataSource.component';
import { GroupCatagorySetupComponent } from '../group-catagory-setup/group-catagory-setup.component';
import { GroupSetupComponent } from '../group-setup/group-setup.component';

@Component({
  selector: 'app-tree-view-exemple',
  templateUrl: './tree-view-exemple.component.html',
  styleUrls: ['./tree-view-exemple.component.css']
})
export class TreeViewExempleComponent implements OnInit {

  // tslint:disable: variable-name
  // tslint:disable: no-
  rootNode: any;
  reloadComponent: Subject<boolean> = new Subject<boolean>();
  isGroupSelected = false;
  groupId: number = 0;
  parentId: number | null;
  groupCode: string = '';
  groupCatagories: GroupCatagory[] = [];
  groupsubCatagories: GroupCatagory[] = [];
  groups: Group[] = [];
  treeControl: FlatTreeControl<DynamicFlatNode>;
  dataSource: DynamicDataSource;
  showGroup = false;
  getLevel = (node: DynamicFlatNode) => node.level;
  isExpandable = (node: DynamicFlatNode) => node.expandable;
  hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;
  isCategory = (_: number, _nodeData: DynamicFlatNode) => _nodeData.data?.isGroupCategory;
  selectedGroupCategoryNode: DynamicFlatNode | null;
  selectedGroupCategory: GroupCatagory;
  selectedGroupNode: DynamicFlatNode | null;
  constructor(
    private groupCatagoryService: GroupCategoryService,
    private groupService: GroupService,
    private route: Router,
    private database: DynamicDatabase,
    public dialog: MatDialog,
  ) {
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
  }

  ngOnInit() {
    this.database = new DynamicDatabase(this.groupService, this.groupCatagoryService);
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new DynamicDataSource(this.treeControl, this.database);
    this.dataSource.data = this.database.initialData('Mat Tree view');
    if (this.dataSource.data[0].item === ' ') {
      this.dataSource.data[0].item = 'Mat Tree view';
      this.selectRootNodeAsSelectedGroupCategory();
    }

    this.showGroup = true;
  }
  selectRootNodeAsSelectedGroupCategory() {
    this.parentId = null;
    this.selectedGroupCategoryNode = this.dataSource.data[0];
  }

  processSelectedGroup() {
    if (!this.isGroupSelected) {
      return;
    }

    let node = this.selectedGroupNode;
    this.parentId = null;
    if (node?.data == null) {
      return;
    }

    var groupNode = node.data;

    if (groupNode.data == null) {
      return;
    }

    var group = groupNode.data;
    this.groupId = group.groupId;


    // this.uiSyncService.onGroupSelected.next(group);
  }

  selectGroup(node: any) {
    this.selectedGroupNode = node;
    this.selectedGroupCategoryNode = null;
    this.isGroupSelected = true;

    this.processSelectedGroup();
    //this.route.navigate(['/all-deliveries', group.id]);
  }

  selectGroupCategory(node: any) {
    if (node.data == null) {
      return;
    }

    if (!node.data.isGroupCategory) {
      return;
    }

    this.selectedGroupCategoryNode = node;
    this.selectedGroupNode = null;
    this.resetGroupSelection();

    var categoryNode = node.data;

    if (categoryNode == null || categoryNode.data == null) {
      if (this.parentId as number > 0) {
        this.parentId = 0;
      }
      return;
    }

    var groupCategory = categoryNode.data;

    this.selectedGroupCategory = groupCategory;
    this.parentId = groupCategory.id;
    this.groupCode = groupCategory.code;

  }

  resetGroupSelection() {

    this.selectedGroupNode = null;
    this.isGroupSelected = false;
  }

  addCatagory() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.maxWidth = '80%';
    dialogConfig.width = '700px';
    dialogConfig.data = { parentId: this.parentId, code: this.groupCode };

    const dialogRef = this.dialog.open(GroupCatagorySetupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(groupCategory => {
      if (groupCategory !== 'Cancel') {
        if (this.selectedGroupCategoryNode == null) {
          this.selectRootNodeAsSelectedGroupCategory();
          this.parentId = null;
        }

        if (this.selectedGroupCategoryNode != null) {
          this.loadNewGroupCategoryToTree(this.selectedGroupCategoryNode, groupCategory);
        }
      }
    });
  }

  addGroup() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.maxWidth = '80%';
    dialogConfig.data = { groupCatagoryId: this.parentId, code: this.groupCode };
    const dialogRef = this.dialog.open(GroupSetupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(group => {

      if (group !== 'Cancel') {
        if (this.selectedGroupCategoryNode == null) {
          this.selectRootNodeAsSelectedGroupCategory();
          this.parentId = null;
        }

        if (this.selectedGroupCategoryNode != null) {
          this.loadNewGroupToTree(this.selectedGroupCategoryNode, group);

        }

      }
    });
  }

  loadNewGroupToTree(selectedGroupCategoryNode: DynamicFlatNode, group: Group) {

    var groupNode = this.database.getGroupNode(group);
    var childNodes = this.database.data.get(selectedGroupCategoryNode.item);
    if (childNodes == null) {
      this.database.data.set(selectedGroupCategoryNode.item, [groupNode]);
    }
    else {
      childNodes.push(groupNode);
    }
    selectedGroupCategoryNode.expandable = true;
    this.dataSource.toggleNode(selectedGroupCategoryNode, true, true);

  }

  loadNewGroupCategoryToTree(selectedGroupCategoryNode: DynamicFlatNode, groupCategory: GroupCatagory) {

    var groupCategoryNode = this.database.getCatagoryNode(groupCategory);
    var childNodes = this.database.data.get(selectedGroupCategoryNode.item);
    if (childNodes == null) {

      this.database.data.set(selectedGroupCategoryNode.item, [groupCategoryNode]);
    }
    else {
      const index = childNodes.filter(c => c.isGroupCategory);
      console.log('index' + index.length);
      childNodes.splice(index.length, 0, groupCategoryNode);
    }
    selectedGroupCategoryNode.expandable = true;
    this.dataSource.toggleNode(selectedGroupCategoryNode, true, true);


  }

}
