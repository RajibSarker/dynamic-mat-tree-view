import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
import { map } from 'rxjs/operators';
import { DynamicFlatNode } from '../_models/DynamicFlatNode';
import { DynamicDatabase } from '../_services/DynamicDatabase';

export class DynamicDataSource implements DataSource<DynamicFlatNode> {
    // tslint:disable: variable-name
    dataChange = new BehaviorSubject<DynamicFlatNode[]>([]);

    get data(): DynamicFlatNode[] { return this.dataChange.value; }
    set data(value: DynamicFlatNode[]) {
        console.log(value);
        this._treeControl.dataNodes = value;
        this.dataChange.next(value);
    }

    constructor(private _treeControl: FlatTreeControl<DynamicFlatNode>,
        private _database: DynamicDatabase) { }

    connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {
        this._treeControl.expansionModel.changed.subscribe(change => {
            if ((change as SelectionChange<DynamicFlatNode>).added ||
                (change as SelectionChange<DynamicFlatNode>).removed) {
                this.handleTreeControl(change as SelectionChange<DynamicFlatNode>);
            }
        });

        return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
    }

    disconnect(collectionViewer: CollectionViewer): void { }

    /** Handle expand/collapse behaviors */
    handleTreeControl(change: SelectionChange<DynamicFlatNode>) {
        if (change.added) {
            change.added.forEach(node => this.toggleNode(node, true));
        } 
        if (change.removed) {
            change.removed.slice().reverse().forEach(node => this.toggleNode(node, false));
        }
    }

    /**
     * Toggle the node, remove from display list
     */
    toggleNode(node: DynamicFlatNode, expand: boolean, isReload?: boolean) {
        const children = this._database.getChildren(node.item);
        const index = this.data.indexOf(node);
        if (!children || index < 0) { // If no children, or cannot find the node, no op
            return;
        }

        node.isLoading = true;

        setTimeout(() => {
            if (expand) {
                console.log('Data: ', this.data);
                const nodes = children.map(groupNCategoryNode =>
                    new DynamicFlatNode(groupNCategoryNode.name, node.level + 1, this._database.isExpandable(groupNCategoryNode.name), groupNCategoryNode));

                //fiter out existing data of same level 
                let level = node.level + 1;
                let levelData = this.data.filter(c => c.level == level);
                if (isReload) {
                    if (levelData.length > 0) {
                        // console.log(this.data.length && this.data[index + 1].level > node.level);
                        // this.data.splice(index + 1, levelData.length);

                        // let parentId = this.data[node.level].data?.data?.id;
                        // this.recusive(parentId);

                        let count = 0;
                        for (let i = index + 1; i < this.data.length
                            && this.data[i].level > node.level; i++, count++) { }
                        this.data.splice(index + 1, count);
                    }
                }
                this.data.splice(index + 1, 0, ...nodes);
                node.isExpanded = true;

            } else {
                let count = 0;
                for (let i = index + 1; i < this.data.length
                    && this.data[i].level > node.level; i++, count++) { }
                this.data.splice(index + 1, count);
                node.isExpanded = false;
            }

            // notify the change
            this.dataChange.next(this.data);
            node.isLoading = false;
        }, 1000);
    }

    recusive(parentId: number) {
        if (parentId === 0) {
            return;
        }
        else {
            var dd = this.data.find(c => c.data?.data?.parentId === parentId);
            if (dd) {
                let index = this.data.findIndex(c => c.data?.name === dd?.data?.name);
                this.data.splice(index, 1);
                this.recusive(dd.data?.data.id);
            }
            return;
        }
    }
}
